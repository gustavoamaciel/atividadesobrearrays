﻿using System;

namespace AtividadeSobreArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            RodarPrograma();
        }

        static Cliente[] SolicitarInformacoes(int qtdClientes)
        {
            Cliente[] clientes = new Cliente[qtdClientes];
            for (int i = 0; i < qtdClientes; i++) {
                Console.WriteLine("Por favor insira suas informações:");
                Console.Write("Nome: ");
                string nome = Console.ReadLine();
                Console.Write("Idade: ");
                int idade = Convert.ToInt32(Console.ReadLine());
                Console.Write("Email: ");
                string email = Console.ReadLine();
                Cliente cliente = new Cliente(nome, idade, email);
                clientes[i] = cliente;
                Console.Clear();
            }
            
            return clientes;
        }

        static void ExibirInformacoes(Cliente[] clientes)
        {
            foreach (Cliente cliente in clientes)
            {
                Console.WriteLine(cliente.ToString());
            }
        }

        static void BuscarCliente(Cliente[] clientes)
        {
            Console.Clear();
            Console.Write("Digite o nome do cliente desejado:");
            string nome = Console.ReadLine();
            foreach (Cliente cliente in clientes)
            {
                if (nome == cliente.Nome)
                {
                    Console.WriteLine("Cliente encontrando!");
                    Console.WriteLine(cliente.ToString());
                    return;
                } 
            }
            Console.WriteLine("Cliente não encontrado!");
        }

        static void RodarPrograma()
        {
            Cliente[] clientes = new Cliente[] { };
            do
            {
                int opcao = EscolherOpcao();

                switch (opcao)
                {
                    case 1:
                        Console.Clear();
                        clientes = SolicitarInformacoes(3);
                        Console.WriteLine("Clientes cadastrados!");
                        Console.WriteLine("");
                        break;
                    case 2:
                        ExibirInformacoes(clientes);
                        break;
                    case 3:
                        BuscarCliente(clientes);
                        break;
                    case 4:
                        return;
                    default:
                        return;
                    
                }

            } while (Console.ReadKey().Key == ConsoleKey.Enter);
        }

        static int EscolherOpcao()
        {
            Console.WriteLine("Bem vindo ao gerenciamento de cliente!");
            Console.WriteLine("Para cadastrar clientes digite: 1");
            Console.WriteLine("Para exibir todos clientes digite: 2");
            Console.WriteLine("Para buscar um cliente digite: 3");
            Console.WriteLine("Para sair digite: 4");
            int opcao = Convert.ToInt32(Console.ReadLine());

            return opcao;
        }


    }
}
