Com base na classe cliente:

public class Cliente

    {


        public string Nome { get; set; }


        public int Idade { get; set; }


        public string Email { get; set; }


        public override string ToString()


        {


            return $"{Nome} | {Idade} | {Email}";


        }


    }

1 - Criar um Array de Clientes, onde devem ser armazenados 3 clientes, onde:

A) Para cada um deles, o usuário deve informar o nome, a idade e o e-mail.

B) Depois, todas as informações de todos os clientes devem ser exibidas.

2 – Separar, na classe Program, dois novos métodos responsáveis por obter as informações do cliente, onde ele faça a primeira parte da questão (letra A) (lembrar que devem ser estáticos – ex.: static void SolicitarInformacoes()); Não esqueça de chamar este novo método a partir do método Main.

3 - Separar, na classe Program, dois novos métodos responsáveis por exibir as informações do cliente, onde ele faça a primeira parte da questão (letra B) (lembrar que devem ser estáticos – ex.: static void ExibirInformacoes()); Não esqueça de chamar este novo método a partir do método Main.

4- Crie um novo método, responsável por “buscar” um determinado cliente, onde, primeiro é questionado o nome do cliente (clienteProcurado), e a cada cliente listado (utilizando o laço “for”), existe uma verificação (if) para ver se o nome do cliente listado é igual ao cliente procurado (clienteProcurado).

5 – Insira, no método Main, uma estrutura de repetição do tipo “do/while” que pergunte a opção desejada, entre os métodos criados anteriormente, e dependendo da resposta do usuário, o método específico seja selecionado (pode ser utilizado o if ou o switch/case).

Ex.:

do

            {





            }while(Console.ReadKey().Key == ConsoleKey.Enter);
